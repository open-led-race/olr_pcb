EESchema Schematic File Version 4
LIBS:openLEDrace_AD20_pub-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L openLEDrace_AD20_pub-rescue:Audio-Jack-3_2Switches-openLEDrace1-rescue J4
U 1 1 5CA0AADA
P 2250 3150
F 0 "J4" H 2250 3440 50  0000 C CNN
F 1 "Audio-Jack-3_2Switches" H 1900 2900 50  0000 L CNN
F 2 "footprints:Tayda_3.5mm_stereo_TRS_jack_A-853" H 2500 3250 50  0001 C CNN
F 3 "" H 2500 3250 50  0001 C CNN
	1    2250 3150
	1    0    0    -1  
$EndComp
$Comp
L openLEDrace_AD20_pub-rescue:Audio-Jack-3_2Switches-openLEDrace1-rescue J5
U 1 1 5CA0AB3E
P 2250 3850
F 0 "J5" H 2250 4140 50  0000 C CNN
F 1 "Audio-Jack-3_2Switches" H 1850 3550 50  0000 L CNN
F 2 "footprints:Tayda_3.5mm_stereo_TRS_jack_A-853" H 2500 3950 50  0001 C CNN
F 3 "" H 2500 3950 50  0001 C CNN
	1    2250 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3050 4450 3050
Wire Wire Line
	2900 3350 2050 3350
Wire Wire Line
	2900 4100 2050 4100
Wire Wire Line
	2050 4100 2050 4050
Wire Wire Line
	2450 3850 4400 3850
$Comp
L power:GNDREF #PWR08
U 1 1 5CA0CBCF
P 2900 4250
F 0 "#PWR08" H 2900 4000 50  0001 C CNN
F 1 "GNDREF" H 2900 4100 50  0001 C CNN
F 2 "" H 2900 4250 50  0001 C CNN
F 3 "" H 2900 4250 50  0001 C CNN
	1    2900 4250
	1    0    0    -1  
$EndComp
Connection ~ 2900 4100
Text Notes 7150 6900 0    157  ~ 0
Open LED Race AD20 Edition
Text Notes 7150 7100 0    60   ~ 0
(CC- BY-SA) 2019  Gerardo A. Barbarov Rostan - Singular Devices Maker Studio
Wire Wire Line
	2900 3350 2900 4100
Wire Wire Line
	2900 4100 2900 4250
$Comp
L openLEDrace_AD20_pub-rescue:Arduino_Nano_v3.x-MCU_Module-openLEDrace1-rescue IC1
U 1 1 5D5E665C
P 5500 3500
F 0 "IC1" H 5500 2411 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 5200 4600 50  0000 C CNN
F 2 "footprints:Arduino_Nano" H 5650 2550 50  0001 L CNN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 5500 2500 50  0001 C CNN
	1    5500 3500
	-1   0    0    1   
$EndComp
$Comp
L openLEDrace_AD20_pub-rescue:CP-Device-openLEDrace1-rescue C2
U 1 1 5D5FF996
P 7650 1950
F 0 "C2" H 7768 1996 50  0000 L CNN
F 1 "1000 uF" H 7768 1905 50  0000 L CNN
F 2 "footprints:CP_Radial_D8.0mm_P5.00mm" H 7688 1800 50  0001 C CNN
F 3 "~" H 7650 1950 50  0001 C CNN
	1    7650 1950
	1    0    0    -1  
$EndComp
$Comp
L openLEDrace_AD20_pub-rescue:C-Device-openLEDrace1-rescue C1
U 1 1 5D600578
P 7250 3350
F 0 "C1" V 7400 3300 50  0000 L CNN
F 1 "1 uF" V 7100 3250 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D4.0mm_P2.00mm" H 7288 3200 50  0001 C CNN
F 3 "~" H 7250 3350 50  0001 C CNN
	1    7250 3350
	0    -1   -1   0   
$EndComp
$Comp
L openLEDrace_AD20_pub-rescue:R-Device-openLEDrace1-rescue R1
U 1 1 5D60094F
P 7200 2300
F 0 "R1" V 7300 2300 50  0000 L CNN
F 1 "470 R" V 7100 2200 50  0000 L CNN
F 2 "Resistors_Universal:Resistor_SMD+THTuniversal_1206_RM10_HandSoldering_RevA_Date25Jun2010" V 7130 2300 50  0001 C CNN
F 3 "~" H 7200 2300 50  0001 C CNN
	1    7200 2300
	0    -1   -1   0   
$EndComp
$Comp
L openLEDrace_AD20_pub-rescue:Conn_02x05_Odd_Even-Connector_Generic-openLEDrace1-rescue J1
U 1 1 5D601849
P 8050 5000
F 0 "J1" H 8100 5417 50  0000 C CNN
F 1 "Conn_02x05_Top_Bottom" H 8150 4700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x05_Pitch2.54mm" H 8050 5000 50  0001 C CNN
F 3 "~" H 8050 5000 50  0001 C CNN
	1    8050 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR0101
U 1 1 5D642992
P 9200 2700
F 0 "#PWR0101" H 9200 2450 50  0001 C CNN
F 1 "GNDREF" H 9200 2550 50  0001 C CNN
F 2 "" H 9200 2700 50  0001 C CNN
F 3 "" H 9200 2700 50  0001 C CNN
	1    9200 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR0102
U 1 1 5D64403F
P 7650 2100
F 0 "#PWR0102" H 7650 1850 50  0001 C CNN
F 1 "GNDREF" H 7650 1950 50  0001 C CNN
F 2 "" H 7650 2100 50  0001 C CNN
F 3 "" H 7650 2100 50  0001 C CNN
	1    7650 2100
	1    0    0    -1  
$EndComp
$Comp
L openLEDrace_AD20_pub-rescue:Conn_01x02_Female-Connector-openLEDrace1-rescue J3
U 1 1 5D645E74
P 9400 3000
F 0 "J3" H 9428 2976 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9428 2885 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 9400 3000 50  0001 C CNN
F 3 "~" H 9400 3000 50  0001 C CNN
	1    9400 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR0103
U 1 1 5D647CB5
P 9200 3250
F 0 "#PWR0103" H 9200 3000 50  0001 C CNN
F 1 "GNDREF" H 9200 3100 50  0001 C CNN
F 2 "" H 9200 3250 50  0001 C CNN
F 3 "" H 9200 3250 50  0001 C CNN
	1    9200 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 3250 9200 3100
Wire Wire Line
	6750 3800 6000 3800
Wire Wire Line
	7050 2300 6550 2300
Wire Wire Line
	6550 2300 6550 3900
Wire Wire Line
	6550 3900 6000 3900
Wire Wire Line
	6300 1800 6300 4700
Wire Wire Line
	5300 4700 5300 4500
Wire Wire Line
	4400 3850 4400 3500
Wire Wire Line
	4400 3500 5000 3500
Wire Wire Line
	4300 3750 4300 3400
Wire Wire Line
	4300 3400 5000 3400
Wire Wire Line
	2450 3750 4300 3750
Wire Wire Line
	4300 3150 4300 3300
Wire Wire Line
	4300 3300 5000 3300
Wire Wire Line
	2450 3150 4300 3150
Wire Wire Line
	4450 3050 4450 3200
Wire Wire Line
	4450 3200 5000 3200
Wire Wire Line
	5500 2500 5450 2500
Wire Wire Line
	5450 2500 5450 2050
Wire Wire Line
	5450 2050 4750 2050
Wire Wire Line
	4750 2050 4750 2550
Connection ~ 5450 2500
Wire Wire Line
	5450 2500 5400 2500
$Comp
L power:GNDREF #PWR0104
U 1 1 5D64FAFC
P 4750 2550
F 0 "#PWR0104" H 4750 2300 50  0001 C CNN
F 1 "GNDREF" H 4750 2400 50  0001 C CNN
F 2 "" H 4750 2550 50  0001 C CNN
F 3 "" H 4750 2550 50  0001 C CNN
	1    4750 2550
	1    0    0    -1  
$EndComp
Text Notes 7650 5700 0    50   ~ 0
https://en.wikipedia.org/wiki/UEXT
$Comp
L power:GNDREF #PWR0105
U 1 1 5D6533F9
P 10000 5550
F 0 "#PWR0105" H 10000 5300 50  0001 C CNN
F 1 "GNDREF" H 10000 5400 50  0001 C CNN
F 2 "" H 10000 5550 50  0001 C CNN
F 3 "" H 10000 5550 50  0001 C CNN
	1    10000 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4100 6000 4100
Wire Wire Line
	7200 4000 6000 4000
Wire Wire Line
	4800 3000 5000 3000
Wire Wire Line
	8350 4800 8350 4550
Wire Wire Line
	4900 3100 5000 3100
Wire Wire Line
	6950 2900 6000 2900
Wire Wire Line
	8750 4150 7500 4150
Wire Wire Line
	7500 4150 7500 3600
Wire Wire Line
	7500 3600 7050 3600
Wire Wire Line
	7400 3000 7400 3350
Wire Wire Line
	7100 3350 6750 3350
Wire Wire Line
	6750 3350 6750 3800
Wire Wire Line
	7050 3000 6000 3000
Wire Wire Line
	7050 3000 7050 3600
Wire Wire Line
	6000 2800 7700 2800
Wire Wire Line
	9100 3850 6900 3850
Wire Wire Line
	6900 3850 6900 3100
Wire Wire Line
	6900 3100 6000 3100
Wire Wire Line
	7400 3000 9200 3000
Wire Wire Line
	7850 4800 6450 4800
Wire Wire Line
	7850 4900 7200 4900
Wire Wire Line
	7200 4900 7200 4000
Wire Wire Line
	8350 4900 8550 4900
Wire Wire Line
	8550 4900 8550 4400
Wire Wire Line
	8550 4400 7050 4400
Wire Wire Line
	7050 4400 7050 4100
Wire Wire Line
	8350 4550 10000 4550
Wire Wire Line
	10000 4550 10000 5550
Wire Wire Line
	7850 5000 4800 5000
Wire Wire Line
	4800 5000 4800 3000
Wire Wire Line
	8350 5000 9500 5000
Wire Wire Line
	9500 5000 9500 5550
Wire Wire Line
	9500 5550 4900 5550
Wire Wire Line
	4900 3100 4900 5550
Wire Wire Line
	7850 5100 6950 5100
Wire Wire Line
	6950 2900 6950 5100
Wire Wire Line
	8350 5100 8750 5100
Wire Wire Line
	8750 5100 8750 4150
Wire Wire Line
	7850 5200 7700 5200
Wire Wire Line
	7700 2800 7700 5200
Wire Wire Line
	8350 5200 9100 5200
Wire Wire Line
	9100 5200 9100 3850
$Comp
L SparkFun-Resistors:RESISTOR0805 R2
U 1 1 5D7C9316
P 5650 5950
F 0 "R2" H 5500 5900 45  0000 C CNN
F 1 "3.3V OUT UEXT" H 5650 6100 45  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5650 6100 20  0001 C CNN
F 3 "" H 5650 5950 60  0001 C CNN
F 4 " " H 5650 6071 60  0000 C CNN "Campo4"
	1    5650 5950
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:RESISTOR0805 R3
U 1 1 5D7CA4FB
P 5650 6200
F 0 "R3" H 5550 6150 45  0000 C CNN
F 1 "5V OUT UEXT  DEFAULT ( WARNING NOT STANDART)" H 5800 5800 45  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5650 6350 20  0001 C CNN
F 3 "" H 5650 6200 60  0001 C CNN
F 4 " " H 5650 6321 60  0000 C CNN "Campo4"
	1    5650 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 5950 5850 6100
Wire Wire Line
	5850 6100 6450 6100
Wire Wire Line
	6450 6100 6450 4800
Connection ~ 5850 6100
Wire Wire Line
	5850 6100 5850 6200
Wire Wire Line
	5400 5950 5450 5950
Wire Wire Line
	5400 4500 5400 5950
Wire Wire Line
	5300 4700 5300 6200
Wire Wire Line
	5300 6200 5450 6200
Connection ~ 5300 4700
Wire Wire Line
	5300 6200 5300 6450
Wire Wire Line
	5850 6450 5850 6200
Wire Wire Line
	5300 6450 5850 6450
Connection ~ 5300 6200
Connection ~ 5850 6200
Wire Wire Line
	5300 4700 6300 4700
Wire Wire Line
	6300 1800 7650 1800
Connection ~ 7650 1800
Wire Wire Line
	7650 1800 9200 1800
$Comp
L SparkFun-Connectors:CONN_05 J2
U 1 1 5E316385
P 9400 2100
F 0 "J2" H 9172 2205 45  0000 R CNN
F 1 "CONN_05" H 9172 2289 45  0000 R CNN
F 2 "Connectors_JST:JST_EH_S05B-EH_05x2.50mm_Angled" H 9400 2700 20  0001 C CNN
F 3 "" H 9400 2100 50  0001 C CNN
F 4 "XXX-00000" H 9172 2384 60  0000 R CNN "Campo4"
	1    9400 2100
	-1   0    0    1   
$EndComp
Wire Wire Line
	9300 2100 9200 2100
Wire Wire Line
	9200 2100 9200 1800
Wire Wire Line
	9300 2500 9200 2500
Wire Wire Line
	9200 2700 9200 2500
Wire Wire Line
	7350 2300 9300 2300
Wire Wire Line
	6000 3600 6150 3600
Wire Wire Line
	6150 3600 6150 2450
Wire Wire Line
	6150 2450 8250 2450
Wire Wire Line
	8250 2450 8250 2200
Wire Wire Line
	8250 2200 9300 2200
Wire Wire Line
	9300 2400 8450 2400
Wire Wire Line
	8450 2400 8450 2650
Wire Wire Line
	8450 2650 6600 2650
Wire Wire Line
	6000 3700 6600 3700
Wire Wire Line
	6600 2650 6600 3700
Wire Notes Line
	6750 5700 6750 6900
Wire Notes Line
	6750 6900 4750 6900
Wire Notes Line
	4750 6900 4750 5700
Wire Notes Line
	4750 5700 6750 5700
Text Notes 4950 7000 0    50   ~ 0
Optional SMD 0 R for UEXT Vout select
$EndSCHEMATC
